using System;
using System.Data.SqlClient;

namespace CreditFileInquiryReports
{
	public class ConnClass
	{
		private string server;

		private string userid;

		private string password;

		private string database;

		private SqlConnection sqlConn;

		public SqlConnection ConnProperty
		{
			get
			{
				this.sqlConn = new SqlConnection(string.Concat(new string[] { "Server =", this.server, "; Database =", this.database, "; Integrated Security = True;" }));
				return this.sqlConn;
			}
		}

		public ConnClass()
		{
			this.server = "Voyager";
			this.password = "";
			this.userid = "webuser";
			this.database = "ceps_prod";
		}

		public ConnClass(string srv, string usrid, string pass, string db)
		{
			this.server = srv;
			this.userid = usrid;
			this.password = pass;
			this.database = db;
		}
	}
}