using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace CreditFileInquiryReports.Properties
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance;

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string BCCEmails
		{
			get
			{
				return (string)this["BCCEmails"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string CCEmails
		{
			get
			{
				return (string)this["CCEmails"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Data Source=Voyager;Initial Catalog=ceps_prod;Integrated Security=True")]
		[SpecialSetting(SpecialSetting.ConnectionString)]
		public string ceps_prodConnectionString
		{
			get
			{
				return (string)this["ceps_prodConnectionString"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("ceps_prod")]
		public string DataBaseName
		{
			get
			{
				return (string)this["DataBaseName"];
			}
		}

		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("twtest@lumbermens.ca")]
		public string From
		{
			get
			{
				return (string)this["From"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Yob59740")]
		public string FromPwd
		{
			get
			{
				return (string)this["FromPwd"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("True")]
		public bool Integrated
		{
			get
			{
				return (bool)this["Integrated"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string Password
		{
			get
			{
				return (string)this["Password"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("d:\\temp")]
		public string ReportDestDir
		{
			get
			{
				return (string)this["ReportDestDir"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("d:\\lumbermens\\ReportOutput")]
		public string ReportOutputDir
		{
			get
			{
				return (string)this["ReportOutputDir"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("Voyager")]
		public string ServerName
		{
			get
			{
				return (string)this["ServerName"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("smtp.office365.com")]
		public string SMTP
		{
			get
			{
				return (string)this["SMTP"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("587")]
		public int SMTPPort
		{
			get
			{
				return (int)this["SMTPPort"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		public bool Test
		{
			get
			{
				return (bool)this["Test"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("zjacob@thinkwise.cloud")]
		public string TestToAddr
		{
			get
			{
				return (string)this["TestToAddr"];
			}
		}

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		public string UserId
		{
			get
			{
				return (string)this["UserId"];
			}
		}

		static Settings()
		{
			Settings.defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		}

		public Settings()
		{
		}
	}
}