﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditFileInquiryReports.Helpers
{
    public static class LogHelper
    {
        //private static readonly string mailreccipient = ConfigurationManager.AppSettings["errorLogEmail"];

        public static void LogError(Exception ex)
        {
            string Job = "Credit File Updates";
            string body = ex.Message;
            if(ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
            {
                body += " INNER EXCEPTION: "+ ex.InnerException.Message ;
            }
            Log.Information("ERROR: " + body + " STACK: " + ex.StackTrace + " LOCATION: " + Job);
        }
        public static void LogSuccess(string message)
        {
            string Job = "Credit File Updates";
            Log.Information("SUCCESS: " + message + " LOCATION: " + Job);
        }

    }
}
