using Serilog;
using System;
using System.Windows.Forms;

namespace CreditFileInquiryReports
{
	internal static class Program
	{
		[STAThread]
		private static void Main(string[] args)
		{
			Log.Logger = new LoggerConfiguration()
						   .WriteTo.File($"C:\\CreditUpdateLogs\\Logs\\" + DateTime.Now.ToString("dddd, dd MMMM HHmm") + ".log")
						   .CreateLogger();

			int n;
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			if (((int)args.Length != 1 ? false : !string.IsNullOrEmpty(args[0])))
			{
				if (args[0].ToLower() == "-start")
				{
					Application.Run(new CreditFileInquiryReports(true));
				}
			}
			else if (((int)args.Length != 2 || string.IsNullOrEmpty(args[0]) ? true : string.IsNullOrEmpty(args[1])))
			{
				Application.Run(new CreditFileInquiryReports(false));
			}
			else
			{
				bool isNumeric = int.TryParse(args[1], out n);
				if ((args[0].ToLower() == "-start") & isNumeric)
				{
					Application.Run(new CreditFileInquiryReports(true, n));
				}
			}
		}
	}
}