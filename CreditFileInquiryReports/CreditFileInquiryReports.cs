using CEPSEM2;
using CreditFileInquiryReports.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using CreditFileInquiryReports.Helpers;

namespace CreditFileInquiryReports
{
    public class CreditFileInquiryReports : Form
	{
		private bool b_cmdLine = false;

		private int threads = 10;

		public string user_ID = "0";

		private IContainer components = null;

		private DateTimePicker dateTimePickerFrom;

		private DateTimePicker dateTimePickerTo;

		private Label label1;

		private Label label2;

		private Button btnCreatePDF;

		private CheckBox chkCreatePDF;

		private CheckBox chkSendEmails;

		private Label label3;

		private TextBox txtThreads;

		private ListBox listBox1;

		private ProgressBar progressBar1;

		private BackgroundWorker backgroundWorker1;

		private Label ResultText;

		private Button CancelAsyncBtn;

		public string User_Id
		{
			get
			{
				return this.User_Id;
			}
			set
			{
				this.user_ID = value;
			}
		}

		public CreditFileInquiryReports()
		{
			this.InitializeComponent();
			this.backgroundWorker1.WorkerReportsProgress = true;
			this.backgroundWorker1.WorkerSupportsCancellation = true;
		}

		public CreditFileInquiryReports(bool cmdLine)
		{
			this.InitializeComponent();
			this.backgroundWorker1.WorkerReportsProgress = true;
			this.backgroundWorker1.WorkerSupportsCancellation = true;
			this.b_cmdLine = cmdLine;
			if (this.b_cmdLine)
			{
				this.StartAsyncBtn_Click(this, null);
			}
		}

		public CreditFileInquiryReports(bool cmdLine, int nThreads)
		{
			this.InitializeComponent();
			this.backgroundWorker1.WorkerReportsProgress = true;
			this.backgroundWorker1.WorkerSupportsCancellation = true;
			this.b_cmdLine = cmdLine;
			if (this.b_cmdLine)
			{
				this.StartAsyncBtn_Click(this, null);
			}
			this.threads = nThreads;
		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker worker = sender as BackgroundWorker;
			try
            {				
				Thread.CurrentThread.Priority = ThreadPriority.Highest;
				Thread.CurrentThread.IsBackground = false;
				string strReporDestDir = Settings.Default.ReportDestDir;
				string strReportOutputDir = Settings.Default.ReportOutputDir;

				worker.ReportProgress(0, "Starting Email Process");
				if (this.chkSendEmails.Checked)
				{
					this.SendEmails(worker, int.Parse(this.user_ID), int.Parse(e.Argument.ToString()));
				}
				worker.ReportProgress(100, "Email Process - Complete");

				//worker.ReportProgress(0, "Moving files to  Folder");
				//CreditFileInquiryReports.ProcessXcopy(strReportOutputDir, strReporDestDir);
				//worker.ReportProgress(100, "Moving files to  Folder - Complete");				
			}
			catch(Exception ex)
            {
				LogHelper.LogError(ex);
            }
			if (worker.CancellationPending)
			{
				e.Cancel = true;
			}
			if (this.b_cmdLine)
			{
				Application.Exit();
			}
		}

		private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			Label resultText = this.ResultText;
			int progressPercentage = e.ProgressPercentage;
			resultText.Text = string.Concat(progressPercentage.ToString(), "%");
			if (e.UserState != null)
			{
				this.listBox1.Items.Add(e.UserState.ToString());
			}
			this.progressBar1.Value = e.ProgressPercentage;
		}

		private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Cancelled)
			{
				this.ResultText.Text = "Canceled!";
			}
			else if (e.Error == null)
			{
				this.progressBar1.Value = 100;
				this.ResultText.Text = "Done!";
			}
			else
			{
				this.ResultText.Text = string.Concat("Error: ", e.Error.Message);
			}
		}

		private void CancelAsyncBtn_Click(object sender, EventArgs e)
		{
			if (this.backgroundWorker1.WorkerSupportsCancellation)
			{
				this.backgroundWorker1.CancelAsync();
			}
		}

		private string CheckQuickView(ConnClass conn, Dictionary<string, string> Session)
		{
			SqlDataReader dr = null;
			string ret = "N";
			SqlConnection cn = conn.ConnProperty;
			int timeout_sec = 600;
			SqlCommand cmd = new SqlCommand("sel_rep_quick_view", cn)
			{
				CommandType = CommandType.StoredProcedure
			};
			SqlParameter SqlParm = cmd.Parameters.Add("@subject_no", SqlDbType.VarChar, 8);
			SqlParm.Value = Session["var_subject_no"];
			cmd.CommandTimeout = timeout_sec;
			cn.Open();
			dr = cmd.ExecuteReader();
			if (Session["var_section"].ToString().IndexOf("2") > -1)
			{
				ret = "Y";
			}
			if (Session["var_section"].ToString().IndexOf("K") > -1)
			{
				ret = "Y";
			}
			if (Session["var_section"].ToString().IndexOf("M") > -1)
			{
				ret = "Y";
			}
			if (!dr.Read())
			{
				ret = "N";
			}
			dr.Close();
			cn.Close();
			return ret;
		}

		private void CheckSection(ConnClass Conn, CreditFileInquiry cfi, Dictionary<string, string> Session)
		{
			try
			{
				string sections = cfi.INQ_COMMENT.Replace("Legal Item", "P");
				sections = sections.Replace("Bank Info", "J");
				sections = sections.Replace("Corp Search", "FG");
				sections = sections.Replace("PPSA Search", "N");
				sections = sections.Replace("Plaintiff Legal", "P");
				sections = sections.Replace("Trade Info", "M");
				sections = sections.Replace(",", "");
				Session["var_section"] = string.Concat("BE", sections);
			}
			catch (Exception exception)
			{
				LogHelper.LogError(exception);
			}
		}

		public void CleanHTMLFolders()
		{
			string strReportOutputDir = Settings.Default.ReportOutputDir;
			string[] directories = Directory.GetDirectories(strReportOutputDir, "temp*");
			for (int i = 0; i < (int)directories.Length; i++)
			{
				string dir = directories[i];
				string[] htmlFiles = Directory.GetFiles(dir, "*.*");
				string foldername = dir.Replace(string.Concat(strReportOutputDir, "\\"), "");
				string[] strArrays = htmlFiles;
				for (int j = 0; j < (int)strArrays.Length; j++)
				{
					string htmlFile = strArrays[j];
					string newHtmlFile = htmlFile.Replace(string.Concat(foldername, "\\"), "");
					newHtmlFile = newHtmlFile.Replace(".html", ".htm");
					try
					{
						File.Copy(htmlFile, newHtmlFile);
					}
					catch (Exception exception)
					{
						LogHelper.LogError(exception);
					}
					File.Delete(htmlFile);
				}
				string[] imgFiles = Directory.GetFiles(string.Concat(dir, "\\images"), "*.*");
				string[] strArrays1 = imgFiles;
				for (int k = 0; k < (int)strArrays1.Length; k++)
				{
					string imgFile = strArrays1[k];
					string newImgFile = imgFile.Replace(string.Concat(foldername, "\\"), "");
					try
					{
						File.Copy(imgFile, newImgFile);
					}
					catch (Exception exception1)
					{
						LogHelper.LogError(exception1);
					}
					File.Delete(imgFile);
				}
				try
				{
					Directory.Delete(dir, true);
				}
				catch (Exception exception2)
				{
					LogHelper.LogError(exception2);
				}
			}
		}

	
		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void EmptyFolder(DirectoryInfo directoryInfo)
		{
			FileInfo[] files = directoryInfo.GetFiles();
			for (int i = 0; i < (int)files.Length; i++)
			{
				files[i].Delete();
			}
			DirectoryInfo[] directories = directoryInfo.GetDirectories();
			for (int j = 0; j < (int)directories.Length; j++)
			{
				this.EmptyFolder(directories[j]);
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			this.SetDateDefaults();
			this.txtThreads.Text = this.threads.ToString();
		}

		private void InitializeComponent()
		{
			this.dateTimePickerFrom = new DateTimePicker();
			this.dateTimePickerTo = new DateTimePicker();
			this.label1 = new Label();
			this.label2 = new Label();
			this.btnCreatePDF = new Button();
			this.chkCreatePDF = new CheckBox();
			this.chkSendEmails = new CheckBox();
			this.label3 = new Label();
			this.txtThreads = new TextBox();
			this.listBox1 = new ListBox();
			this.progressBar1 = new ProgressBar();
			this.backgroundWorker1 = new BackgroundWorker();
			this.ResultText = new Label();
			this.CancelAsyncBtn = new Button();
			base.SuspendLayout();
			this.dateTimePickerFrom.Location = new Point(68, 14);
			this.dateTimePickerFrom.Name = "dateTimePickerFrom";
			this.dateTimePickerFrom.Size = new System.Drawing.Size(200, 20);
			this.dateTimePickerFrom.TabIndex = 1;
			this.dateTimePickerTo.Location = new Point(345, 14);
			this.dateTimePickerTo.Name = "dateTimePickerTo";
			this.dateTimePickerTo.Size = new System.Drawing.Size(200, 20);
			this.dateTimePickerTo.TabIndex = 2;
			this.label1.AutoSize = true;
			this.label1.Location = new Point(316, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(23, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "To:";
			this.label2.AutoSize = true;
			this.label2.Location = new Point(29, 20);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(33, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "From:";
			this.btnCreatePDF.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.btnCreatePDF.Location = new Point(522, 217);
			this.btnCreatePDF.Name = "btnCreatePDF";
			this.btnCreatePDF.Size = new System.Drawing.Size(187, 32);
			this.btnCreatePDF.TabIndex = 5;
			this.btnCreatePDF.Text = "Create Credit File Updates";
			this.btnCreatePDF.UseVisualStyleBackColor = true;
			this.btnCreatePDF.Click += new EventHandler(this.StartAsyncBtn_Click);
			this.chkCreatePDF.AutoSize = true;
			this.chkCreatePDF.Checked = true;
			this.chkCreatePDF.CheckState = CheckState.Checked;
			this.chkCreatePDF.Location = new Point(430, 89);
			this.chkCreatePDF.Name = "chkCreatePDF";
			this.chkCreatePDF.Size = new System.Drawing.Size(135, 17);
			this.chkCreatePDF.TabIndex = 6;
			this.chkCreatePDF.Text = "Create PDF and HTML";
			this.chkCreatePDF.UseVisualStyleBackColor = true;
			this.chkSendEmails.AutoSize = true;
			this.chkSendEmails.Checked = true;
			this.chkSendEmails.CheckState = CheckState.Checked;
			this.chkSendEmails.Location = new Point(602, 89);
			this.chkSendEmails.Name = "chkSendEmails";
			this.chkSendEmails.Size = new System.Drawing.Size(84, 17);
			this.chkSendEmails.TabIndex = 7;
			this.chkSendEmails.Text = "Send Emails";
			this.chkSendEmails.UseVisualStyleBackColor = true;
			this.label3.AutoSize = true;
			this.label3.Location = new Point(692, 90);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 13);
			this.label3.TabIndex = 11;
			this.label3.Text = "Threads";
			this.txtThreads.Location = new Point(744, 86);
			this.txtThreads.Name = "txtThreads";
			this.txtThreads.Size = new System.Drawing.Size(42, 20);
			this.txtThreads.TabIndex = 10;
			this.txtThreads.Text = "10";
			this.listBox1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new Point(3, 112);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(792, 95);
			this.listBox1.TabIndex = 9;
			this.progressBar1.Location = new Point(32, 83);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(380, 23);
			this.progressBar1.TabIndex = 8;
			this.backgroundWorker1.DoWork += new DoWorkEventHandler(this.backgroundWorker1_DoWork);
			this.backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
			this.backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
			this.ResultText.AutoSize = true;
			this.ResultText.Location = new Point(37, 57);
			this.ResultText.Name = "ResultText";
			this.ResultText.Size = new System.Drawing.Size(0, 13);
			this.ResultText.TabIndex = 12;
			this.CancelAsyncBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.CancelAsyncBtn.Location = new Point(715, 217);
			this.CancelAsyncBtn.Name = "CancelAsyncBtn";
			this.CancelAsyncBtn.Size = new System.Drawing.Size(75, 32);
			this.CancelAsyncBtn.TabIndex = 13;
			this.CancelAsyncBtn.Text = "Cancel";
			this.CancelAsyncBtn.UseVisualStyleBackColor = true;
			this.CancelAsyncBtn.Click += new EventHandler(this.CancelAsyncBtn_Click);
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(798, 261);
			base.Controls.Add(this.CancelAsyncBtn);
			base.Controls.Add(this.ResultText);
			base.Controls.Add(this.label3);
			base.Controls.Add(this.txtThreads);
			base.Controls.Add(this.listBox1);
			base.Controls.Add(this.progressBar1);
			base.Controls.Add(this.chkSendEmails);
			base.Controls.Add(this.chkCreatePDF);
			base.Controls.Add(this.btnCreatePDF);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.dateTimePickerTo);
			base.Controls.Add(this.dateTimePickerFrom);
			base.Name = "CreditFileInquiryReports";
			this.Text = "Credit File Update";
			base.Load += new EventHandler(this.Form1_Load);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private static void MemberNo(Dictionary<string, string> Session, ConnClass conn, out string memberNo)
		{
			int timeout_sec = 600;
			SqlConnection cn = conn.ConnProperty;
			cn.Open();
			SqlCommand cmd = new SqlCommand("sel_member_no_by_inq", cn)
			{
				CommandType = CommandType.StoredProcedure
			};
			SqlParameter SqlParm = cmd.Parameters.Add("@var_inquiry_no", SqlDbType.VarChar, 6);
			SqlParm.Value = Session["var_inquiry_no"];
			cmd.CommandTimeout = timeout_sec;
			memberNo = cmd.ExecuteScalar().ToString();
			cn.Close();
		}

		private static void ProcessXcopy(string SolutionDirectory, string TargetDirectory)
		{
			ProcessStartInfo startInfo = new ProcessStartInfo()
			{
				CreateNoWindow = false,
				UseShellExecute = false,
				FileName = "xcopy",
				WindowStyle = ProcessWindowStyle.Hidden,
				Arguments = string.Concat(new string[] { "\"", SolutionDirectory, "\" \"", TargetDirectory, "\" /e /y /I" })
			};
			try
			{
				using (Process exeProcess = Process.Start(startInfo))
				{
					exeProcess.WaitForExit();
				}
			}
			catch (Exception exception)
			{
				LogHelper.LogError(exception);
				throw exception;
			}
		}

		private void SendEmails(BackgroundWorker worker, int user_id, int threads)
		{
			using (ceps_prodEntities context = new ceps_prodEntities())
			{
				context.Database.CommandTimeout = new int?(600);
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("<!DOCTYPE html>");
				stringBuilder.Append("<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">");
				stringBuilder.Append("<head runat=\"server\">");
				stringBuilder.Append("    <meta charset=\"utf-8\" />");
				stringBuilder.Append("    <title></title>");
				stringBuilder.Append("    <asp:ContentPlaceHolder ID=\"head\" runat=\"server\" />");
				stringBuilder.Append("    <style type=\"text/css\">");
				stringBuilder.Append("        p.MsoNormal {");
				stringBuilder.Append("            margin-bottom: .0001pt;");
				stringBuilder.Append("            font-size: 11.0pt;");
				stringBuilder.Append("            font-family: \"Calibri\",sans-serif;");
				stringBuilder.Append("            margin-left: 0cm;");
				stringBuilder.Append("            margin-right: 0cm;");
				stringBuilder.Append("            margin-top: 0cm;");
				stringBuilder.Append("        }");
				stringBuilder.Append("        table.MsoNormalTable {");
				stringBuilder.Append("            font-size: 11.0pt;");
				stringBuilder.Append("            font-family: \"Calibri\",sans-serif;");
				stringBuilder.Append("        }");
				stringBuilder.Append("    </style>");
				stringBuilder.Append("</head>");
				stringBuilder.Append("<body>[TESTMESSAGE]");
				stringBuilder.Append("    <p class=\"MsoNormal\">");
				stringBuilder.Append("        <span >Please see attached Credit Files Updates on the following Subject(s):<o:p></o:p></span>");
				stringBuilder.Append("    </p>");
				stringBuilder.Append("    <table border=\"1\" cellpadding=\"0\" class=\"MsoNormalTable\" style=\"mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184\">");
				stringBuilder.Append("        <thead>");
				stringBuilder.Append("            <tr >");
				stringBuilder.Append("                <td style=\"padding: .75pt .75pt .75pt .75pt\">");
				stringBuilder.Append("                    <p  class=\"MsoNormal\" style=\"text-align: center\">");
				stringBuilder.Append("                        <b><span  style=\"font-size: 13.5pt;\">Subject Name</span></b>");
				stringBuilder.Append("                    </p>");
				stringBuilder.Append("                </td>");
				stringBuilder.Append("                <td style=\"padding: .75pt .75pt .75pt .75pt\">");
				stringBuilder.Append("                    <p class=\"MsoNormal\" style=\"text-align: center\">");
				stringBuilder.Append("                        <b><span style=\"font-size: 13.5pt;\">File</span></b>");
				stringBuilder.Append("                    </p>");
				stringBuilder.Append("                </td>");
				stringBuilder.Append("                <td style=\"padding: .75pt .75pt .75pt .75pt\">");
				stringBuilder.Append("                    <p class=\"MsoNormal\" style=\"text-align: center\">");
				stringBuilder.Append("                        <b><span style=\"font-size: 13.5pt;\">Update Triggered By</span></b>");
				stringBuilder.Append("                    </p>");
				stringBuilder.Append("                </td>");
				stringBuilder.Append("            </tr>");
				stringBuilder.Append("        </thead>");
				StringBuilder stringBuilder1 = new StringBuilder();
				stringBuilder1.Append(" <tr>");
				stringBuilder1.Append("            <td style=\"width: 35.0%; padding: .75pt .75pt .75pt .75pt\" >");
				stringBuilder1.Append("                <p class=\"MsoNormal\">[SUBJECTNAME]</p>");
				stringBuilder1.Append("            </td>");
				stringBuilder1.Append("            <td style=\"width: 9.0%; padding: .75pt .75pt .75pt .75pt\" >");
				stringBuilder1.Append("                <p class=\"MsoNormal\">[FILENAME]</p>");
				stringBuilder1.Append("            </td>");
				stringBuilder1.Append("            <td style=\"width: 20.0%; padding: .75pt .75pt .75pt .75pt\" >");
				stringBuilder1.Append("                <p class=\"MsoNormal\">[TRIGGEREDBY]</p>");
				stringBuilder1.Append("            </td>");
				stringBuilder1.Append("        </tr>");
				string str = "</ table ></ body ></ html >";
				List<CreditFileInquiryEmail> list = (
					from cr in context.CreditFileInquiryEmails
					where (cr.INQ_RECEIVE_DATE > this.dateTimePickerFrom.Value) && (cr.INQ_RECEIVE_DATE <= this.dateTimePickerTo.Value)
					select cr).ToList<CreditFileInquiryEmail>();
				var emails = (
					from cr in list
					select new { EMAIL_ADD = cr.EMAIL_ADD }).Distinct().ToList();
				string reportOutputDir = Settings.Default.ReportOutputDir;
				double count = (double)list.Count;
				double num1 = 0;
				object obj = new object();
				ParallelOptions options = new ParallelOptions()
				{
					MaxDegreeOfParallelism = threads
				};
				DateTime start = DateTime.Now;
				worker.ReportProgress(0, string.Concat("Starting Time: ", start.ToLongTimeString()));
				LogHelper.LogSuccess(string.Concat("Email Starting Time: ", start.ToLongTimeString()));

				Parallel.ForEach(emails, options, (email, state) => {

					double num;
					string lasterror = "";
					IEnumerable<CreditFileInquiryEmail> creditFileInquiryEmails = 
						from cr in list
						where cr.EMAIL_ADD.Trim() == email.EMAIL_ADD.Trim()
						select cr;

					Func<CreditFileInquiryEmail, string> inquirySortOrder = (CreditFileInquiryEmail x) => x.INQUIRY_NO;
					
					List<CreditFileInquiryEmail> creditFileEnquiriesByEmail = creditFileInquiryEmails.OrderByDescending<CreditFileInquiryEmail, string>(inquirySortOrder).ToList<CreditFileInquiryEmail>();
					try
					{
						using (SmtpClient smtpClient = new SmtpClient())
                        {
                            string toEmailAddr = email.EMAIL_ADD.ToString();
                            string strCC = "ralph.sampaga@lumbermens.ca";

							MailMessage message = new MailMessage();
							Stream stream = new MemoryStream();

							message.From = new MailAddress(ConfigurationManager.AppSettings["FROM_EMAIL"],ConfigurationManager.AppSettings["FROM_NAME"]);

							message.Bcc.Add("william.petryniak@lumbermens.ca");
							message.To.Add(toEmailAddr);

                            if (!string.IsNullOrEmpty(strCC.Trim()))
                            {
                                string[] strArrays = strCC.Split(",".ToCharArray());
                                for (int i = 0; i < (int)strArrays.Length; i++)
                                {
                                    string ccStr = strArrays[i];
                                    message.CC.Add(ccStr);
                                }
                            }

                            string body = "";
							foreach (CreditFileInquiryEmail attachmentEMAIL in creditFileEnquiriesByEmail)
							{
								string strContents = stringBuilder1.ToString();
								strContents = strContents.Replace("[SUBJECTNAME]", attachmentEMAIL.SUB_NAME1);
								strContents = strContents.Replace("[FILENAME]", attachmentEMAIL.FILE_NAME);
								strContents = strContents.Replace("[TRIGGEREDBY]", attachmentEMAIL.TRIGGEREDBY);
								body = string.Concat(body, strContents);
								string attachmentFilename = string.Concat(reportOutputDir, "\\", attachmentEMAIL.FILE_NAME);
								if (attachmentFilename != null && File.Exists(attachmentFilename))
								{
									stream = File.OpenRead(attachmentFilename);
									Attachment attachment = new Attachment(stream, attachmentEMAIL.FILE_NAME);
									message.Attachments.Add(attachment);
								}
							}

							string formattedHeader = "";
							message.Subject = "Credit File Updates";
							formattedHeader = stringBuilder.ToString().Replace("[TESTMESSAGE]", "");

							message.Body = string.Concat(formattedHeader, body, str);

							AlternateView alternateView = AlternateView.CreateAlternateViewFromString(string.Concat(formattedHeader, body, str), null, "text/html");
							message.AlternateViews.Add(alternateView);

							message.IsBodyHtml = true;

							if (message.Attachments.Count != 0)
                            {								
								smtpClient.Send(message);
								LogHelper.LogSuccess("Credit File Update Sent to: " + toEmailAddr);
							}
							else
                            {
								LogHelper.LogSuccess("No attachments found for email to: " + toEmailAddr);
                            }
							stream.Dispose();
							GC.Collect();
						}										
					}
					catch (Exception exception)
					{
						LogHelper.LogError(exception);
						Exception ex = exception;
						lock (obj)
						{
							lasterror = string.Concat(new object[] { lasterror, email, ":", ex.Message });
							if (ex.InnerException != null)
							{
								lasterror = string.Concat(lasterror, "Inner:", ex.InnerException.Message);
							}
						}
					}
					lock (obj)
					{
						if (worker.CancellationPending)
						{
							state.Break();
						}
						else if (string.IsNullOrEmpty(lasterror))
						{
							BackgroundWorker cSu0024u003cu003e8_locals1 = worker;
							num = num1;
							num1 = num + 1;
							cSu0024u003cu003e8_locals1.ReportProgress((int)(num / count * 100), string.Concat(email, " ", DateTime.Now.ToLongTimeString()));
						}
						else
						{
							BackgroundWorker backgroundWorker = worker;
							num = num1;
							num1 = num + 1;
							backgroundWorker.ReportProgress((int)(num / count * 100), lasterror);
							lasterror = "";
						}
					}
				});
			}
		}

		private void SetDateDefaults()
		{
			DateTime dtNow = DateTime.Now;
			DateTime dtTomm = dtNow.Add(new TimeSpan(24, 0, 0));
			this.dateTimePickerFrom.Value = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day);
			this.dateTimePickerTo.Value = new DateTime(dtTomm.Year, dtTomm.Month, dtTomm.Day);
		}

		private void StartAsyncBtn_Click(object sender, EventArgs e)
		{
			if (!this.backgroundWorker1.IsBusy)
			{
				this.listBox1.Items.Clear();
				this.backgroundWorker1.RunWorkerAsync(this.txtThreads.Text);
			}
		}
	}
}